import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Chris on 20/08/2017.
 */
public class TestS {

    DriverFactory factory = new DriverFactory();
    GooglePageModel googlePageModel;
    //Do this before every test
    @Before
    public void start(){
        googlePageModel = new GooglePageModel(factory.getDriver());
        googlePageModel.goTo();
     }
    //Do this after every test
    @After
    public void close() throws IOException {
        googlePageModel.close();
    }

    @org.junit.Test
    public void SearchForSomthing(){
        googlePageModel.enterSearchText("what is the meaning of life");
        googlePageModel.search();
    }
}
