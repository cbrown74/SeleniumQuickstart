import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import sun.rmi.runtime.Log;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chris on 20/08/2017.
 */
public class GooglePageModel {

    static int count = 0;
    String url = "https://www.google.co.uk/";
    By searchTextField = By.name("q");
    Map < Elem, By > map = new HashMap < Elem, By > ();
    WebDriver driver;

    GooglePageModel(WebDriver driver) {
        this.driver = driver;
        map.put(Elem.SEARCH_TEXT_FIELD, searchTextField);
    }


    public void enterSearchText(String searchText) {
        getElement(Elem.SEARCH_TEXT_FIELD).sendKeys(searchText);
    }

    public void search() {
        getElement(Elem.SEARCH_TEXT_FIELD).submit();
    }

    public void close() throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/target/screenshots/test" + (++count) + ".png"));
        driver.quit();
    }

    public void goTo() {
        driver.get(url);
        driver.manage().window().setSize(new Dimension(1000, 1500));
        System.out.println("Loaded url " + driver.getCurrentUrl());
    }


    public WebElement getElement(Elem name) {
        return driver.findElement(map.get(name));
    }



}